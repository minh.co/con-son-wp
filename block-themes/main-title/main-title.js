import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { registerBlockType } from "@wordpress/blocks";

import { LollypopLogo } from "../../assets/images/logo";

registerBlockType("block-themes/main-title", {
  title: "Main Title",
  category: "lollypop",
  icon: {
    src: LollypopLogo,
    // background: "#ff0",
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const template = [["core/heading", { placeholder: "Your main title" }]];

  const blockProps = useBlockProps({ className: "main-title" });

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template,
  });

  return <div {...innerBlocksProps}></div>;
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return (
    <div className="container">
      <div className="main-title">
        <div {...innerBlocksProps}></div>
      </div>
    </div>
  );
}
