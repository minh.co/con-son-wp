import { registerBlockType } from "@wordpress/blocks";
import { LollypopLogo } from "../../../assets/images/logo";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper";
import {
  InspectorControls,
  MediaUpload,
  MediaUploadCheck,
  useBlockProps,
  useInnerBlocksProps,
  __experimentalImageSizeControl as ImageSizeControl,
} from "@wordpress/block-editor";
import {
  PanelBody,
  ResizableBox,
  RangeControl,
  ToggleControl,
} from "@wordpress/components";
import { Icon, plusCircle } from "@wordpress/icons";
import { useState } from "react";
registerBlockType("block-themes/slider", {
  apiVersion: 2,
  title: "Slider",
  category: "lollypop",
  icon: {
    src: LollypopLogo,
  },
  attributes: {
    image: { type: "array", default: [] },
    perToView: { type: "number", default: 1 },
    showNextPrev: { type: "boolean", default: true },
    showDot: { type: "boolean", default: true },
    spaceBetween: { type: "number", default: 10 },
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent(props) {
  const blockProps = useBlockProps({});

  const innerBlocksProps = useInnerBlocksProps(blockProps);
  const { attributes = {}, setAttributes = () => {} } = props;
  const [show, setShow] = useState(true);
  const {
    image = [],
    perToView,
    showNextPrev,
    spaceBetween,
    showDot,
  } = attributes;

  const handleFileSelect = (file) => {
    setAttributes({
      image: file,
    });
  };

  const MediaButton = (open) => {
    return <Icon onClick={open} className="icon" icon={plusCircle} />;
  };

  return (
    <>
      <InspectorControls>
        <PanelBody>
          <RangeControl
            help="Please select number slide to show"
            initialPosition={perToView}
            label="Per To View"
            max={10}
            min={1}
            steps={1}
            onChange={(perToView) => setAttributes({ perToView })}
          />
        </PanelBody>
        <PanelBody>
          <RangeControl
            help="Space between slide"
            initialPosition={perToView}
            label="Space"
            max={200}
            min={10}
            steps={10}
            onChange={(spaceBetween) => setAttributes({ spaceBetween })}
          />
        </PanelBody>
        <PanelBody title="Show Button next, prev">
          <ToggleControl
            checked={showNextPrev}
            onChange={() => setAttributes({ showNextPrev: !showNextPrev })}
          />
        </PanelBody>
        <PanelBody title="Show Dot">
          <ToggleControl
            checked={showDot}
            onChange={() => setAttributes({ showDot: !showDot })}
          />
        </PanelBody>
      </InspectorControls>
      <div className="imageBlock">
        {image.length === 0 ? (
          <ResizableBox
            size={{ height: 300, width: 300 }}
            className="addImageBox"
          >
            <MediaUploadCheck>
              <MediaUpload
                multiple
                gallery
                onSelect={(file) => handleFileSelect(file)}
                value={1}
                render={({ open }) => MediaButton(open)}
              />
            </MediaUploadCheck>
          </ResizableBox>
        ) : (
          <Swiper
            // install Swiper modules
            loop={true}
            centeredSlides={true}
            modules={[Navigation, Pagination]}
            spaceBetween={spaceBetween}
            slidesPerView={perToView}
            navigation={showNextPrev}
            pagination={showDot ? { clickable: true } : false}
            // onSwiper={(swiper) => console.log(swiper)}
            onSlideChange={() => console.log("slide change")}
          >
            {image.map((item, index) => {
              const { id, url } = item;
              return (
                <SwiperSlide key={id} className={`slide_${index}`}>
                  <img className="image" src={url} alt="image" />
                </SwiperSlide>
              );
            })}
          </Swiper>
        )}
      </div>
    </>
  );
}

function SaveComponent(props) {
  const { attributes = {} } = props;
  const { image, perToView, showNextPrev, showDot, spaceBetween } = attributes;
  return (
    <div
      class="swiper mySwiper"
      data-view={perToView}
      data-space={spaceBetween}
    >
      <div class="swiper-wrapper">
        {image.map((item, index) => {
          return (
            <div class={`swiper-slide slide_${index}`}>
              <img className="image" src={item.url} alt="image" />
            </div>
          );
        })}
      </div>
      {showNextPrev && (
        <>
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </>
      )}
      {showDot && <div class="swiper-pagination"></div>}
    </div>
  );
}
