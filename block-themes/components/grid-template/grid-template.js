import {
  useBlockProps,
  useInnerBlocksProps,
  InspectorControls,
  getColorObjectByColorValue,
} from "@wordpress/block-editor";
import { canLockBlockType } from "@wordpress/block-editor/build/store/selectors.js";
import { registerBlockType } from "@wordpress/blocks";
import {
  __experimentalGrid as Grid,
  PanelRow,
  PanelBody,
  RangeControl,
  __experimentalNumberControl as NumberControl,
  ColorPalette,
} from "@wordpress/components";
import { LollypopLogo } from "../../../assets/images/logo.js";
import "./grid-template.scss";
registerBlockType("block-themes/grid-template", {
  apiVersion: 2,
  title: "Grid Template",
  category: "lollypop",
  icon: LollypopLogo,
  supports: {
    html: false,
    color: {
      background: true,
      text: true,
      gradients: true,
    },
  },
  styles: [
    {
      name: "fit",
      label: "Fit",
      isDefault: true,
    },
    {
      name: "full",
      label: "Full",
    },
  ],
  attributes: {
    columns: { type: "number", default: 3 },
    gap: { type: "number", default: 1 },
    background: { type: "string", default: "none" },
    backgroundName: { type: "string", default: "none" },
    paddingX: { type: "number", default: 0 },
    customClassName: { type: "string", default: "" },
    customStyle: { type: "object" },
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const {
    columns,
    gap,
    backgroundName,
    paddingX,
    paddingY,
    customClassName,
    customStyle,
  } = attributes;

  const ALLOWED_BLOCKS = ["block-themes/card-content"];

  const blockProps = useBlockProps({
    className: `grid-${columns}-columns grid gap-lv${gap} ${customClassName}`,
    style: {
      ...customStyle,
    },
  });
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    allowedBlocks: ALLOWED_BLOCKS,
  });

  const ourColors = [
    { name: "white", color: "#fff" },
    { name: "dark", color: "#221429" },
  ];

  return (
    <>
      <InspectorControls>
        <PanelBody>
          <RangeControl
            help="Please select column in row"
            initialPosition={columns}
            label="Column"
            max={6}
            min={1}
            steps={1}
            onChange={(columns) => setAttributes({ columns })}
          />
        </PanelBody>
        <PanelBody title="Gap" initialOpen={true}>
          <PanelRow>
            <NumberControl
              onChange={(gap) => setAttributes({ gap: Number(gap) })}
              step={1}
              min={1}
              max={100}
              value={gap}
            />
          </PanelRow>
        </PanelBody>
        <PanelBody title="Padding" initialOpen={true}>
          <PanelRow>
            <NumberControl
              label="Padding left and right"
              onChange={(paddingX) =>
                setAttributes({ paddingX: Number(paddingX) })
              }
              step={1}
              min={1}
              max={25}
              value={paddingX}
            />
          </PanelRow>
          <PanelRow>
            <NumberControl
              label="Padding top and bottom"
              onChange={(paddingY) =>
                setAttributes({ paddingY: Number(paddingY) })
              }
              step={1}
              min={1}
              max={25}
              value={paddingY}
            />
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <div
        className={`background-${backgroundName}`}
        style={{ padding: `${paddingY}px ${paddingX}px` }}
      >
        <div className={`grid-${columns}-columns`} {...innerBlocksProps} />
      </div>
    </>
  );
}

function SaveComponent({ attributes }) {
  const {
    columns,
    gap,
    backgroundName,
    paddingX,
    paddingY,
    customClassName,
    customStyle,
  } = attributes;

  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({
      className: `grid-${columns}-columns grid gap-lv${gap} ${customClassName}`,
      style: {
        ...customStyle,
      },
    })
  );

  return (
    <div
      className={`background-${backgroundName}`}
      style={{ padding: `${paddingY}px ${paddingX}px` }}
    >
      <div className={`grid-${columns}-columns`} {...innerBlocksProps} />
    </div>
  );
}
