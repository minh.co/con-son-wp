import { registerBlockType } from "@wordpress/blocks";
import {
  BlockControls,
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  __experimentalUseColorProps as useColorProps,
  __experimentalLinkControl as LinkControl,
  AlignmentToolbar,
  RichText,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
} from "@wordpress/block-editor";
import {
  PanelBody,
  ToolbarGroup,
  ToolbarButton,
  __experimentalBoxControl as BoxControl,
  Popover,
  Button,
  ButtonGroup,
} from "@wordpress/components";
import { link } from "@wordpress/icons";
import { useState } from "react";
import { LollypopLogo } from "../../../assets/images/logo";

registerBlockType("block-themes/button", {
  apiVersion: 2,
  title: "Button",
  category: "lollypop",
  icon: { src: LollypopLogo },
  html: false,
  attributes: {
    linkObject: { type: "object", default: "" },
    placeholder: { type: "string" },
    text: { type: "string", default: "" },
    align: { type: "string", default: "left" },
    padding: {
      type: "object",
      default: { top: "0px", right: "0px", bottom: "0px", left: "0px" },
    },
    width: { type: "string", default: "auto" },
    customClassName: { type: "string", default: "" },
    customStyle: { type: "object" },
  },
  supports: {
    // align: ["left", "right", "center"],
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
    typography: {
      fontSize: true,
      __experimentalFontFamily: true,
      __experimentalTextTransform: true,
      __experimentalDefaultControls: {
        fontSize: true,
      },
    },
  },
  styles: [
    { name: "fill", label: "Fill", isDefault: true },
    { name: "outline", label: "Out Line" },
  ],
  example: {
    text: "Example",
    align: "center",
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const {
    padding,
    linkObject,
    text,
    placeholder,
    align,
    width,
    customClassName,
    customStyle,
  } = attributes;

  const blockProps = useBlockProps({
    className: `conson-button text-${align} ${customClassName}`,
    style: { ...customStyle },
  });

  //get color from support
  const [isLinkPicker, setIsLinkPicker] = useState(false);
  const [isPrimary, setIsPrimary] = useState(["auto", "33.3%", "50%", "100%"]);

  const colorProps = useColorProps(attributes);

  const innerBlocksProps = useInnerBlocksProps(blockProps, {});

  const handleCheck = ({ top, right, bottom, left }) => {
    setAttributes({
      padding: {
        top: top || "0",
        right: right || "0",
        bottom: bottom || "0",
        left: left || "0",
      },
    });
  };
  const handleChangeWidth = (e, index) => {
    let button = document.querySelectorAll(".button-width");
    for (let i = 0; i < button.length; i++) {
      if (button[i].classList.contains("is-primary")) {
        button[i].classList.remove("is-primary");
        button[i].classList.add("is-secondary");
      }
    }
    e.target.classList.remove("is-secondary");
    e.target.classList.add("is-primary");
    let result = e.target.textContent;
    setAttributes({ width: result });
  };
  return (
    <>
      <BlockControls>
        <ToolbarGroup>
          <ToolbarButton
            onClick={() => setIsLinkPicker((prev) => !prev)}
            icon={link}
          ></ToolbarButton>
        </ToolbarGroup>
        <ToolbarGroup>
          <AlignmentToolbar
            value={align}
            alignmentControls={[
              {
                title: "Left",
                icon: "editor-alignleft",
                align: "left",
              },
              {
                title: "Center",
                icon: "editor-aligncenter",
                align: "center",
              },
              {
                title: "Right",
                icon: "editor-alignright",
                align: "right",
              },
            ]}
            onChange={(align) => setAttributes({ align })}
          />
        </ToolbarGroup>
      </BlockControls>
      <InspectorControls>
        <PanelBody title="Padding">
          <BoxControl
            label="Padding"
            // sides={["top", "right", "bottom", "left"]}
            // values={padding}
            defaultValue={{
              top: "0px",
              right: "0px",
              bottom: "0px",
              left: "0px",
            }}
            onChange={(x) => handleCheck(x)}
          />
        </PanelBody>
        <PanelBody title="Width">
          <ButtonGroup>
            {isPrimary.map((val, index) => {
              let isSelect = val === width ? `primary` : `secondary`;
              return (
                <Button
                  className="button-width"
                  variant={isSelect}
                  onClick={(e) => handleChangeWidth(e, index)}
                >
                  {val}
                </Button>
              );
            })}
          </ButtonGroup>
        </PanelBody>
      </InspectorControls>

      <div
        {...innerBlocksProps}
        style={{
          display: "flex",
          justifyContent:
            align === "left"
              ? `flex-start`
              : align === "right"
              ? `flex-end`
              : "center",
        }}
      >
        <button
          className="d-flex justify-content-center align-items-center"
          style={{
            "--background-button":
              colorProps.style.backgroundColor || "#E0D6AF",
            padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`,
            width: width,
          }}
        >
          {linkObject ? (
            <a
              href={linkObject.url}
              style={{
                color: colorProps.style.color,
                fontSize: blockProps.style.fontSize,
              }}
            >
              <RichText
                className={`link`}
                value={text}
                onChange={(text) => setAttributes({ text })}
                allowedFormats={[]}
                placeholder={placeholder || "Add text..."}
              ></RichText>
            </a>
          ) : (
            <span
              style={{
                color: colorProps.style.color,
                fontSize: blockProps.style.fontSize,
              }}
            >
              <RichText
                className={`text`}
                value={text}
                onChange={(text) => setAttributes({ text })}
                allowedFormats={[]}
                placeholder={placeholder || "Add text..."}
              ></RichText>
            </span>
          )}
        </button>
      </div>
      {isLinkPicker && (
        <Popover position="bottom center">
          <LinkControl
            settings={[]}
            value={linkObject}
            onChange={(newLink) => setAttributes({ linkObject: newLink })}
          />
          <Button
            variant="primary"
            onClick={() => setIsLinkPicker(false)}
            style={{ display: "block", width: "100%" }}
          >
            Confirm link
          </Button>
        </Popover>
      )}
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const {
    padding,
    linkObject,
    text,
    placeholder,
    align,
    width,
    customClassName,
    customStyle,
  } = attributes;

  const colorProps = getColorClassesAndStyles(attributes);
  const blockProps = useBlockProps.save();
  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({
      className: `conson-button text-${align} ${customClassName}`,
      style: { ...customStyle },
    })
  );

  return (
    <div
      {...innerBlocksProps}
      style={{
        display: "flex",
        justifyContent:
          align === "left"
            ? `flex-start`
            : align === "right"
            ? `flex-end`
            : "center",
      }}
    >
      <button
        className="d-flex justify-content-center align-items-center"
        style={{
          "--background-button": colorProps.style.backgroundColor || "#E0D6AF",
          padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`,
          width: width,
        }}
      >
        {linkObject ? (
          <a
            href={linkObject.url}
            style={{
              color: colorProps.style.color,
              fontSize: blockProps.style.fontSize,
            }}
          >
            <RichText.Content
              className={`link`}
              value={text}
              onChange={(text) => setAttributes({ text })}
              allowedFormats={[]}
              placeholder={placeholder || "Add text..."}
            ></RichText.Content>
          </a>
        ) : (
          <span
            style={{
              color: colorProps.style.color,
              fontSize: blockProps.style.fontSize,
            }}
          >
            <RichText.Content
              className={`text`}
              value={text}
              onChange={(text) => setAttributes({ text })}
              allowedFormats={[]}
              placeholder={placeholder || "Add text..."}
            ></RichText.Content>
          </span>
        )}
      </button>
    </div>
  );
}
