import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/drink-menu", {
  apiVersion: 2,
  title: "Con Son Drink Menu",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {
    row:{type: "number",default:6},
  },
  supports: {
    className: false,
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes,setAttributes = () => {}}) {
  const blockProps = useBlockProps({});
  const { row } = attributes;
  const cardRow = ["block-themes/flex-template",{alignItems:"center",justify:"between"},[
    ["block-themes/card-content",{width:"224px"},[
        ["core/paragraph", { content:"Pu-erh Tea"}]
    ]],
    ["block-themes/media-image",{ width: 120, height: 40}],
    ["core/paragraph",{content:"$56"}], 
]]

const renderRow = () => {
    return new Array(row).fill(1).map((item,index)=>{
        if(index === 0){
            return [["core/heading", { content: "Tea & Wine" }],
            ["core/spacer", { height: "48px" }]]
        }
        return [cardRow,["core/paragraph",{content:"VI | Bottle"}],["core/spacer", { height: "40px" }]]
    }).flat(1)
} 

const templates = [
    ["block-themes/container-template",{ fullScreen: true },[
        ["block-themes/container-template",{ color: "transparent" },[
            ["core/paragraph", { content: "Menu that fits you palatte" }],
            ["core/spacer", { height: "8px" }],
            ["block-themes/media-image",{ width: 80, height: 80}],
            ["core/spacer", { height: "12px" }],
            ["core/heading", { content: "Today’s Special" }],
            ["core/spacer", { height: "64px" }],
            ["block-themes/flex-template",{justify:"between"},[
                ["block-themes/card-content",{width:"412px"},
                    // ["core/heading", { content: "Tea & Wine" }],
                    // ["core/spacer", { height: "48px" }],
                    // cardRow,
                    // ["core/paragraph",{content:"VI | Bottle"}],
                    // ["core/spacer", { height: "40px" }],
                    // cardRow,
                    // ["core/paragraph",{content:"VI | Bottle"}],
                    // ["core/spacer", { height: "40px" }],
                    // cardRow,
                    // ["core/paragraph",{content:"FR | 750 ml"}],
                    // ["core/spacer", { height: "40px" }],
                    // cardRow,
                    // ["core/paragraph",{content:"CA | 750 ml"}],
                    // ["core/spacer", { height: "40px" }],
                    // cardRow,
                    // ["core/paragraph",{content:"IE | 750 ml"}],
                    // ["core/spacer", { height: "40px" }],
                    renderRow()
            ],
                ["block-themes/card-content",{width:"412px"},[
                    ["block-themes/media-image",{ width: 412, height: 660}],
                ]],
                ["block-themes/card-content",{width:"412px"},
                    renderRow()
            ],
            ]],
            ["core/spacer", { height: "65px"}],
            ["block-themes/button",{text:"View More"}]
          ],
        ],
      ],
    ]
]

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });

  return <div {...innerBlocksProps}></div>
        
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return <div {...innerBlocksProps}></div>;
}
