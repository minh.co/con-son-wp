import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/testimony", {
  apiVersion: 2,
  title: "Con Son Testimony",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {},
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const blockProps = useBlockProps({});

  const flexContent =  [
    "block-themes/flex-template",{},
    [
      ["block-themes/media-image", { width: 190, height: 190 }],
      ["block-themes/card-content",{ width: "444px", height: "210px" },[
          ["core/paragraph",{content:"Lorem ipsum dolor sit amet, consectetur adipiscing sit. auctor sit iaculis in arcu. Vulputate nulla lobortis mauris eget sit. Nulla scelerisque scelerisque congue."}],
          ["core/spacer", { height: "16px" }],
          ["core/heading", { content: "Jane Cooper" }],
          ["core/spacer", { height: "8px" }],
          ["core/paragraph", { content: "Photographer" }],
        ],
      ],
    ],
  ]

  const templates = [
    ["block-themes/container-template",{fullScreen: true},[
      ["block-themes/container-template",{paddingY: "152px"},[
        ["core/paragraph", { content: "Testimony" }],
        ["core/spacer", { height: "8px" }],
        ["block-themes/media-image", { width: 100, height: 100 }],
        ["core/spacer", { height: "12px" }],
        ["core/heading", { content: "Happy customers" }],
        ["core/spacer", { height: "80px" }],
        ["block-themes/flex-template",{justify: "between", alignItems: "start"},[
          flexContent,flexContent,flexContent,flexContent
        ]]
      ]]
    ]]
  ]    
    
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });
  return <div {...innerBlocksProps}></div>;
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());
 
  return <div {...innerBlocksProps}></div>
  
}
