import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/footer", {
  apiVersion: 2,
  title: "Con Son Footer",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {},
  supports: {
    className: false,
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const blockProps = useBlockProps({});
  
  const templates = [
    [
      "block-themes/container-template",
      { fullScreen: true },
      [
        [
          "block-themes/container-template",
          { color: "transparent",paddingY:"130px" },
          [
            [
              "block-themes/flex-template",
              {justify:"between",alignItems:"center"},
              [
                ["block-themes/card-content",{width:"320px"},
                [
                    ["core/heading",{content:"Contact Us"}],
                    ["core/spacer",{height:"24px"}],
                    ["core/paragraph",{content:"17 Tran Quoc Thao, District 2, HCM city"}],
                    ["core/spacer",{height:"8px"}],
                    ["core/paragraph",{content:"+84 9-344-1230"}],
                    ["core/spacer",{height:"4px"}],
                    ["core/paragraph",{content:"+84 9-555-1230"}],
                ]]
                ,
                ["block-themes/card-content",{width:"390px"},
                [
                    ["block-themes/media-image", { width:133, height: 64 }],
                    ["core/spacer", { height: "32px" }],
                    ["core/paragraph",{ content: "Vietnamese food is not only food, it is Vietnamese culture.”"}],
                    ["core/spacer", { height: "16px" }],
                    ["block-themes/media-image", { width: 50, height: 50 }],
                    ["core/spacer", { height: "16px" }],
                    ["block-themes/flex-template",{justify:"center"},
                    [
                      ["block-themes/card-content",{},
                      [
                        ["block-themes/media-image",{width: 50, height: 50,}],
                      ]],
                      ["block-themes/card-content",{},
                      [
                        ["block-themes/media-image",{width: 50, height: 50,}],
                      ]],
                      ["block-themes/card-content",{},
                      [
                        ["block-themes/media-image",{width: 50, height: 50,}],
                      ]],
                    ]]
                ]],
                ["block-themes/card-content",{width:"210px"},[
                    ["core/heading",{content:"Working Hours"}],
                    ["core/spacer",{height:"24px"}],
                    ["core/paragraph",{content:"Monday-Friday:"}],
                    ["core/spacer",{height:"4px"}],
                    ["core/paragraph",{content:"08:00 am -12:00 am"}],
                    ["core/spacer",{height:"8px"}],
                    ["core/paragraph",{content:"Saturday-Sunday:"}],
                    ["core/spacer",{height:"4px"}],
                    ["core/paragraph",{content:"07:00am -11:00 pm"}],
                ]],
            ],
            ],
            ["core/spacer", { height:"80px"}],
            ["core/paragraph",{content:"2022 Conson. All Rights reserved."}],
          ],
        ],
      ],
    ],
  ];

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });

  return <div {...innerBlocksProps}></div>;
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return <div {...innerBlocksProps}></div>;
}
