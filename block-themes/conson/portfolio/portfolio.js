import { registerBlockType, registerBlockStyle } from "@wordpress/blocks";
import {
  BlockControls,
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  AlignmentToolbar,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
  MediaUpload,
  MediaUploadCheck,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  Button,
  ResizableBox,
  Icon,
} from "@wordpress/components";
import { plusCircle } from "@wordpress/icons";
import { Row, Col } from "react-bootstrap";

import { ConSonLogo } from "../../../assets/images/logoConSon";
import BG from "../../../assets/images/BG.png";

registerBlockType("block-themes/portfolio", {
  apiVersion: 2,
  title: "Portfolio",
  category: "conson",
  icon: { src: ConSonLogo },
  html: false,
  attributes: {
    img: { type: "string", default: BG },

    avatar: { type: "string" },
  },
  supports: {
    // align: ["left", "right", "center"],
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
  },

  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const { img, avatar } = attributes;

  const blockProps = useBlockProps({
    className: `content-portfolio`,
  });

  const ALLOWED_BLOCKS = ["core/heading", "core/paragraph"];

  const MY_TEMPLATE = [
    [
      "block-themes/card-content",
      {},
      [
        ["core/heading", { placeholder: "Enter heading..." }],
        ["core/spacer", { height: "76px" }],
        ["core/paragraph", { placeholder: "Enter paragraph..." }],
        ["core/spacer", { height: "64px" }],
        ["core/heading", { placeholder: "Enter name..." }],
        ["core/paragraph", { placeholder: "Enter description..." }],
      ],
    ],
  ];
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    allowedBlocks: ALLOWED_BLOCKS,
    template: MY_TEMPLATE,
  });
  function onFileSelect(x) {
    setAttributes({ img: x.url });
  }
  const handleFileSelect = (file) => {
    console.log(file);
    setAttributes({ avatar: file.url });
  };
  const MediaButton = (open) => {
    return <Icon onClick={open} className="icon" icon={plusCircle} />;
  };
  return (
    <>
      <InspectorControls>
        <PanelBody title="Background" initalOpen={true}>
          <PanelRow>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={onFileSelect}
                value={img}
                render={({ open }) => {
                  return <Button onClick={open}>Choose Image</Button>;
                }}
              />
            </MediaUploadCheck>
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <div
        className="section-portfolio"
        style={{ background: `url('${img}')` }}
      >
        <div className="container">
          <Row className="align-items-center">
            {avatar ? (
              <Col md="5">
                <div className="wrapper-image">
                  <img src={avatar} />
                </div>
              </Col>
            ) : (
              <ResizableBox
                size={{ height: 300, width: 300 }}
                className="add-image-box"
              >
                <MediaUploadCheck>
                  <MediaUpload
                    onSelect={(file) => handleFileSelect(file)}
                    value={1}
                    render={({ open }) => MediaButton(open)}
                  />
                </MediaUploadCheck>
              </ResizableBox>
            )}

            <Col md="7">
              <div
                className="inner-content"
                {...innerBlocksProps}
                style={{ paddingRight: "96px", textAlign: "left" }}
              ></div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const { img, avatar } = attributes;

  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({ className: `content-portfolio` })
  );

  return (
    <>
      <div
        className="section-portfolio"
        style={{ background: `url('${img}')` }}
      >
        <div className="container">
          <div className="align-items-center row">
            <div className="col-md-5">
              <div className="wrapper-image">
                <img src={avatar} />
              </div>
            </div>

            <div className="col-md-7">
              <div className="inner-content" {...innerBlocksProps}></div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
