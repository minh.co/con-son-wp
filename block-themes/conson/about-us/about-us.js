import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { ConSonLogo } from "../../../assets/images/logoConSon";
import chopstick from "../../../assets/images/chopstick.png";
import background from "../../../assets/images/BG.png";

registerBlockType("block-themes/about-us", {
  apiVersion: 2,
  title: "Con Son About Us",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {},
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const blockProps = useBlockProps({});
  const templates = [
    [
      "block-themes/container-template",
      { paddingY: "333px", color: "transparent" },
      [
        [
          "block-themes/flex-template",
          { justify: "between", alignItems: "center" },
          [
            [
              "block-themes/card-content",
              { width: "523px", align: "right" },
              [
                ["core/heading", { content: "About Us" }],
                ["core/spacer", { height: "32px" }],
                ["block-themes/media-image", { width: 50, height: 50 }],
                ["core/spacer", { height: "32px" }],
                [
                  "core/paragraph",
                  {
                    content:
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis pharetra adipiscing ultrices vulputate posuere tristique. In sed odio nec aliquet eu proin mauris et.",
                  },
                ],
                ["core/spacer", { height: "32px" }],
                ["block-themes/button", { text: "Know More", width: "145px" }],
              ],
            ],
            [
              "block-themes/card-content",
              { width: "523px", align: "left" },
              [
                ["core/heading", { content: "About Us" }],
                ["core/spacer", { height: "32px" }],
                ["block-themes/media-image", { width: 50, height: 50 }],
                ["core/spacer", { height: "32px" }],
                [
                  "core/paragraph",
                  {
                    content:
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis pharetra adipiscing ultrices vulputate posuere tristique. In sed odio nec aliquet eu proin mauris et.",
                  },
                ],
                ["core/spacer", { height: "32px" }],
                ["block-themes/button", { text: "Know More", width: "145px" }],
              ],
            ],
          ],
        ],
      ],
    ],
  ];

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });

  return (
    <div
      style={{ height: "959px", backgroundImage: `url('${background}')` }}
      className="Section-about-us position-relative container-fluid"
    >
      <div {...innerBlocksProps}></div>
      <div className="logoImage centerSection"></div>
      <img className="centerSection index" src={chopstick} alt="logo1" />
      <ConSonLogo className="centerSection opacity-25" fill="#ffffff" />
    </div>
  );
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return (
    <div
      style={{ height: "959px", backgroundImage: `url('${background}')` }}
      className="Section-about-us position-relative container-fluid"
    >
      <div {...innerBlocksProps}></div>
      <div className="logoImage centerSection"></div>
      <img className="centerSection zindex-fixed" src={chopstick} alt="logo1" />
      <ConSonLogo className="centerSection opacity-25" fill="#ffffff" />
    </div>
  );
}
