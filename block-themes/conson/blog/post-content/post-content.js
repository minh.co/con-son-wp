import { registerBlockType } from "@wordpress/blocks";
import {
  useBlockProps,
  useInnerBlocksProps,
  InspectorControls,
} from "@wordpress/block-editor";
import { useSelect } from "@wordpress/data";
import {
  Spinner,
  PanelBody,
  PanelRow,
  RadioControl,
} from "@wordpress/components";
import { ConSonLogo } from "../../../../assets/images/logoConSon";

registerBlockType("block-themes/post-content", {
  apiVersion: 2,
  title: "Con Son Post Content",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {
    perPage: { type: "number", default: 3 },
    display: { type: "string", default: "flex" },
  },
  supports: {
    className: false,
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes, setAttributes }) {
  const { data, perPage } = attributes;

  const blockProps = useBlockProps({
    className: "card-content",
  });

  let posts = [];

  const fetchPosts = () => {
    useSelect((select) => {
      posts = select("core").getEntityRecords("postType", "post", {
        per_page: 3,
        _embed: true,
      });
    });

    if (posts !== null) {
      const postsData = posts.map((item) => {
        return {
          id: item.id || 0,
          excerpt: item.excerpt.raw || "None Food Excerpt",
          date: item.date || "day month year",
          title: item.title.raw || " None title",
          author: item._embedded.author[0].name || "None author",
          image: item._embedded[`wp:featuredmedia`],
        };
      });
      console.log(posts);
      posts = [...postsData];
    }
  };

  fetchPosts();

  const innerBlocksProps = useInnerBlocksProps(blockProps, {});

  return (
    <div className="d-flex justify-content-between">
      {/* <InspectorControls>
        <PanelBody title="Justify items" initialOpen={true}>
         
          <RadioControl
            onChange={(display) => setAttributes({ display })}
            options={[
              {
                label: "Space Around",
                value: "around",
              },
              {
                label: "Space Between",
                value: "between",
              },
            ]}
            selected={justify}
          />
          
        </PanelBody>
      </InspectorControls> */}
      {posts !== null ? (
        <>
          {posts.map((post) => {
            const { author, date, excerpt, id, image, title } = post;

            const formatDate = new Date(date);
            const options = { year: "numeric", month: "long", day: "2-digit" };
            const dateTemp = formatDate.toLocaleDateString("en-US", options);
            const src = image?.map((item) => item.source_url);

            return (
              <div key={id}>
                <img src={src} className="card-img-top" alt={title} />
                <div className="card-body">
                  <div className="card-header">
                    <p>{dateTemp}</p>
                    <p>- {author}</p>
                  </div>
                  <h5 style={{ paddingBottom: "32px" }} className="card-title">
                    {title}
                  </h5>
                  <p style={{ paddingBottom: "32px" }} className="card-text">
                    {excerpt}
                  </p>
                  <a href="#" className=" stretched-link">
                    Read More
                  </a>
                </div>
              </div>
            );
          })}
        </>
      ) : (
        <>
          <div
            className="position-relative"
            style={{ width: "412px", height: "500px" }}
          >
            <div className="centerSection">
              <Spinner
                style={{
                  height: "calc(4px * 20)",
                  width: "calc(4px * 20)",
                }}
              />
            </div>
          </div>
          <div
            className="position-relative"
            style={{ width: "412px", height: "500px" }}
          >
            <div className="centerSection">
              <Spinner
                style={{
                  height: "calc(4px * 20)",
                  width: "calc(4px * 20)",
                }}
              />
            </div>
          </div>
          <div
            className="position-relative"
            style={{ width: "412px", height: "500px" }}
          >
            <div className="centerSection">
              <Spinner
                style={{
                  height: "calc(4px * 20)",
                  width: "calc(4px * 20)",
                }}
              />
            </div>
          </div>
        </>
      )}

      {/* {postContent ? <> {postContent.map((item) => {
            const formatDate = new Date(date);
            const options = { year: "numeric", month: "long", day: "2-digit" };
            const dateTemp = formatDate.toLocaleDateString("en-US", options);
            return (
              <div key={id} {...innerBlocksProps}>
                <img src="..." className="card-img-top" alt="..." />
                <div className="card-body">
                  <div
                    style={{ padding: "24px 0 32px 0" }}
                    className="d-flex justify-content-between"
                  >
                    <p>{dateTemp}</p>
                    <p>{author[0].name || "author"}</p>
                  </div>
                  <h5 style={{ paddingBottom: "32px" }} className="card-title">
                    {title.raw}
                  </h5>
                  <p style={{ paddingBottom: "32px" }} className="card-text">
                    {excerpt.raw}
                  </p>
                  <a href="#" className=" stretched-link">
                    Read More
                  </a>
                </div>
              </div>
           
          })} 
          </>
        
      ) : (
        <Spinner
          style={{
            height: "calc(4px * 20)",
            width: "calc(4px * 20)",
          }}
        />
      )} */}

      {/* postContent.map((item) => {
          const {
            id = 1,
            excerpt = "None Food Excerpt",
            date = "day month year",
            detail = {
              author: [],
            },
            title = "None Title",
          } = item;
          const formatDate = new Date(date);
          const options = { year: "numeric", month: "long", day: "2-digit" };
          console.log(formatDate.toLocaleDateString("en-US", options));
          return (
            <div key={id} {...innerBlocksProps}>
              <img src="..." className="card-img-top" alt="..." />
              <div className="card-body">
                <div
                  style={{ padding: "24px 0 32px 0" }}
                  className="d-flex justify-content-between"
                >
                  <p>{date}</p>
                  <p>{detail && detail.author[0].name}</p>
                </div>
                <h5 style={{ paddingBottom: "32px" }} className="card-title">
                  {title.raw}
                </h5>
                <p style={{ paddingBottom: "32px" }} className="card-text">
                  {excerpt.raw}
                </p>
                <a href="#" className=" stretched-link">
                  Read More
                </a>
              </div>
            </div>
          );
        })} */}
    </div>
  );
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return <div {...innerBlocksProps}></div>;
}
