import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";

import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/blog", {
  apiVersion: 2,
  title: "Con Son BLog",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {},
  supports: {
    className: false,
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const blockProps = useBlockProps({});

  const cardContent = [
    "block-themes/flex-template",
    {},
    [
      [
        "block-themes/media-image",
        { width: 79, height: 79, customClassName: "" },
      ],
      [
        "block-themes/card-content",
        {},
        [
          ["core/paragraph", { content: "Bib Gourmond" }],
          ["core/spacer", { height: "8px" }],
          [
            "core/paragraph",
            { content: "Lorem ipsum dolor sit amet, consectetur." },
          ],
        ],
      ],
    ],
  ];

  const templates = [
    [
      "block-themes/container-template",
      { fullScreen: true },
      [
        [
          "block-themes/container-template",
          { color: "transparent" },
          [
            ["core/paragraph", { content: "Blogs" }],
            ["block-themes/media-image"],
            ["core/spacer", { height: "12px" }],
            ["core/heading", { content: "Conson updates" }],
            ["core/spacer", { height: "64px" }],
            ["block-themes/grid-template"],
          ],
        ],
      ],
    ],
  ];

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });

  return <div {...innerBlocksProps}></div>;
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return <div {...innerBlocksProps}></div>;
}
