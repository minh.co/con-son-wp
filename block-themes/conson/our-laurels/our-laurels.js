import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/our-laurels", {
  apiVersion: 2,
  title: "Con Son Our Laurels",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {},
  supports: {
    className: false,
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const blockProps = useBlockProps({});

  const cardContent = [
    "block-themes/flex-template",
    {},
    [
      [
        "block-themes/media-image",
        { width: 79, height: 79, customClassName: "" },
      ],
      [
        "block-themes/card-content",
        {},
        [
          ["core/paragraph", { content: "Bib Gourmond" }],
          ["core/spacer", { height: "8px" }],
          [
            "core/paragraph",
            { content: "Lorem ipsum dolor sit amet, consectetur." },
          ],
        ],
      ],
    ],
  ];

  const templates = [
    [
      "block-themes/container-template",
      { fullScreen: true },
      [
        [
          "block-themes/container-template",
          { color: "transparent" },
          [
            [
              "block-themes/flex-template",
              {},
              [
                [
                  "block-themes/card-content",
                  {},
                  [
                    ["core/paragraph", { content: "Awards & recognition" }],
                    ["block-themes/media-image", { customClassName: "" }],
                    ["core/spacer", { height: "12px" }],
                    ["core/heading", { content: "Our Laurels" }],
                    ["core/spacer", { height: "64px" }],
                    [
                      "block-themes/grid-template",
                      { columns: 2, gap: 4 },
                      [cardContent, cardContent, cardContent, cardContent],
                    ],
                  ],
                ],
                ["block-themes/image-right", {}],
              ],
            ],
          ],
        ],
      ],
    ],
  ];

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });

  return <div {...innerBlocksProps}></div>;
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return <div {...innerBlocksProps}></div>;
}
