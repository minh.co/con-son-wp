import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { ConSonLogo } from "../../../../assets/images/logoConSon.js";
import left from "../../../../assets/images/left.png";

registerBlockType("block-themes/image-right", {
  apiVersion: 2,
  title: "Laurels-image-right",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {
    color: { type: "string", default: "#040404" },
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const blockProps = useBlockProps({});

  const templates = [
    ["block-themes/media-image", { width: 560, height: 690, align: "end" }],
  ];

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });

  return (
    <>
      <div className="Image-right">
        <img
          className="position-absolute bottom-0 "
          style={{ zIndex: 10, left: "-75px" }}
          src={left}
          alt="banner-image"
        />
        <div {...innerBlocksProps}></div>
      </div>
    </>
  );
}

function SaveComponent() {
  const blockProps = useBlockProps.save({
    // className:
  });

  const innerBlocksProps = useInnerBlocksProps.save(blockProps);

  return (
    <div className="Image-right">
      <img
        className="position-absolute bottom-0 "
        style={{ zIndex: 10, left: "-75px" }}
        src={left}
        alt="banner-image"
      />
      <div {...innerBlocksProps}></div>
    </div>
  );
}
