import { registerBlockType} from "@wordpress/blocks";
import {
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  MediaUpload,
  MediaUploadCheck,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  Button,

} from "@wordpress/components";



import { ConSonLogo } from "../../../assets/images/logoConSon";
import BG from "../../../assets/images/BG.png";
import spoon from "../../../assets/images/spoon.svg";


registerBlockType("block-themes/book-table", {
  apiVersion: 2,
  title: "Book Table",
  category: "conson",
  icon: { src: ConSonLogo },
  html: false,
  attributes: {
    background: { type: "string", default: BG },
  },
  supports: {
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
  },

  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const { background } = attributes;

  const blockProps = useBlockProps({});

  const ALLOWED_BLOCKS = ["core/heading", "core/paragraph"];
  let today = new Date();

  let date = today.toJSON().slice(0,10);
  let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  const MY_TEMPLATE = [
    ["block-themes/container-template",{customClassName:"border-newletter"},[
      ["block-themes/card-content",{},[
        ["core/paragraph",{content:"Reservations"}],
        ["core/spacer",{height:"8px"}],
        ["block-themes/media-image", { image: { url: spoon },width:"40px", height:"9px" }],
        ["core/spacer",{height:"8px"}],
        ["core/heading", {content:"Book A Table"}],
      ]],
      ["core/spacer",{height:"64px"}],
      ["core/columns",{columns:3},[
        ["core/column",{width:"33.3333333%"},[
          ["block-themes/select",{width:"100%",padding:{top:"21px",right:"21px",bottom:"21px",left:"21px"}}]
        ]],
        ["core/column",{width:"33.3333333%"},[
          ["block-themes/input",{ typeInput:"date", value:date, width:"100%", padding:{top:"16px",right:"16px",bottom:"16px",left:"16px"}}]
        ]],
        ["core/column",{width:"33.3333333%"},[
          ["block-themes/input",{ typeInput:"time", value:time, width:"100%",padding:{top:"16px",right:"16px",bottom:"16px",left:"16px"}}]
        ]]
      ]],
      ["core/spacer",{height:"64px"}],
      ["block-themes/button",{text:"Book Now",padding:{top:"8px",right:"32px",bottom:"8px",left:"32px"},align:"center"}]  
    ]],
  ];
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    allowedBlocks: ALLOWED_BLOCKS,
    template: MY_TEMPLATE,
  });
  function onFileSelect(x) {
    setAttributes({ background: x.url });
  }
  
  return (
    <>
      <InspectorControls>
        <PanelBody title="Background" initalOpen={true}>
          <PanelRow>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={onFileSelect}
                value={background}
                render={({ open }) => {
                  return <Button onClick={open}>Choose Image</Button>;
                }}
              />
            </MediaUploadCheck>
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <section
        className="section-book-table"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          // height: "900px",
        }}
      >
        <div {...innerBlocksProps}></div>
      </section>
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const { background } = attributes;

  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({ className: `` })
  );

  return (
    <>
        <section
        className="section-book-table"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          // height: "900px",
        }}
      >
        <div {...innerBlocksProps}></div>
      </section>
    </>
  );
}
