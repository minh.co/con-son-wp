import { registerBlockType } from "@wordpress/blocks";
import { useBlockProps, useInnerBlocksProps } from "@wordpress/block-editor";
import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/header", {
  apiVersion: 2,
  title: "Con Son Header",
  category: "conson",
  icon: {
    src: <ConSonLogo fill="black" />,
  },
  attributes: {},
  supports: {
    className: false,
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const blockProps = useBlockProps({});
  
  const templates = [
    [
      "block-themes/container-template",
      { fullScreen: true,backgroundColor:"#0C0C0C" },
      [
        ["block-themes/card-content",{},[
            ["block-themes/flex-template",{justify:"between",alignItems:"center"},[
                ["block-themes/card-content",{},[
                    ["block-themes/media-image",{width:143,height:64}],
                ]],
                ["block-themes/card-content",{width:"412px"},[
                    ["core/navigation",{textColor:"#FFFFFF"}],
                ]],
                ["block-themes/card-content",{width:"364px"},[
                    ["block-themes/flex-template",{},[
                        ["core/paragraph",{content:"Log in / registration"}],
                        ["core/paragraph",{content:"|"}],
                        ["core/paragraph",{content:"Book table"}],
                    ]],
                ]],
            ]]
        ]]
      ],
    ],
  ];

  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    template: templates,
  });

  return <div {...innerBlocksProps}></div>;
}

function SaveComponent() {
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return <div {...innerBlocksProps}></div>;
}
