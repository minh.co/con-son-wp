import { registerBlockType, registerBlockStyle } from "@wordpress/blocks";
import {
  BlockControls,
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
  MediaUpload,
  MediaUploadCheck,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  Button,
} from "@wordpress/components";
import { plusCircle } from "@wordpress/icons";


import { ConSonLogo } from "../../../assets/images/logoConSon";
import BG from "../../../assets/images/BG.png";
import spoon from "../../../assets/images/spoon.svg";

registerBlockType("block-themes/newletter", {
  apiVersion: 2,
  title: "New Letter",
  category: "conson",
  icon: { src: ConSonLogo },
  html: false,
  attributes: {
    background: { type: "string", default: BG },
  },
  supports: {
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
  },

  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const { background, show } = attributes;

  const blockProps = useBlockProps({});

  const ALLOWED_BLOCKS = ["core/heading", "core/paragraph"];

  const MY_TEMPLATE = [
    ["block-themes/container-template",{customClassName:"d-flex justify-content-center border-newletter"},[
      ["block-themes/card-content",{width:"50%"},[
        ["core/paragraph", { placeholder: "Enter small title..." }],
        ["core/spacer", { height:"8px" }],
        ["block-themes/media-image", { image: { url: spoon } }],
        ["core/spacer", { height: "10px" }],
        ["core/heading", { placeholder: "Enter heading..." }],
        ["core/spacer", { height: "24px" }],
        ["core/paragraph", { placeholder: "Enter small content..." }],
        ["core/spacer", { height:"64px" }],
        ["core/columns",{columns:2},[
            ["core/column",{width:"70%"},[
          ["block-themes/input", { type: "text", placeholder:"Enter email...", width:"100%" }],
            ]],
            ["core/column",{width:"30%"},[
              ["block-themes/button", { placeholder: "Enter small content...", width:"100%" }],
            ]]
        ]]
      ]],
      
    ]],
  ];
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    allowedBlocks: ALLOWED_BLOCKS,
    template: MY_TEMPLATE,
  });
  function onFileSelect(x) {
    setAttributes({ background: x.url });
  }

  return (
    <>
      <InspectorControls>
        <PanelBody title="Background" initalOpen={true}>
          <PanelRow>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={onFileSelect}
                value={background}
                render={({ open }) => {
                  return <Button onClick={open}>Choose Image</Button>;
                }}
              />
            </MediaUploadCheck>
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <section
        className="section-newletter"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          // height: "900px",
        }}
      >
        <div {...innerBlocksProps}></div>
      </section>
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const { background } = attributes;

  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({ className: `` })
  );

  return (
    <>
       <section
        className="section-newletter"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          // height: "900px",
        }}
      >
        <div {...innerBlocksProps}></div>
      </section>
    </>
  );
}
