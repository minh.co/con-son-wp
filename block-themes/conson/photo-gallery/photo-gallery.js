import { registerBlockType, registerBlockStyle } from "@wordpress/blocks";
import {
  BlockControls,
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  AlignmentToolbar,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
  MediaUpload,
  MediaUploadCheck,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  Button,
  ResizableBox,
  Icon,
} from "@wordpress/components";
import { plusCircle } from "@wordpress/icons";
import { Row, Col } from "react-bootstrap";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper";

import { ConSonLogo } from "../../../assets/images/logoConSon";
import BG from "../../../assets/images/BG.png";
import spoon from "../../../assets/images/spoon.svg";
registerBlockType("block-themes/photo-gallery", {
  apiVersion: 2,
  title: "Photo Gallery",
  category: "conson",
  icon: { src: ConSonLogo },
  html: false,
  attributes: {
    background: { type: "string", default: BG },
    image: { type: "array", default: [] },
  },
  supports: {
    align: ["full"],
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
  },

  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const { background, image } = attributes;

  const blockProps = useBlockProps({ className: `content-gallery` });

  // const ALLOWED_BLOCKS = ["core/video"];

  const MY_TEMPLATE = [
    [
      "block-themes/card-content",
      {},
      [
        ["core/paragraph", { placeholder: "Enter small title..." }],
        ["block-themes/media-image", { image: { url: spoon } }],
        ["core/spacer", { height: "10px" }],
        ["core/heading", { placeholder: "Enter heading..." }],
        ["core/spacer", { height: "24px" }],
        ["core/paragraph", { placeholder: "Enter content..." }],
        ["core/spacer", { height: "24px" }],
        ["block-themes/button", {}],
      ],
    ],
  ];
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    // allowedBlocks: ALLOWED_BLOCKS,
    template: MY_TEMPLATE,
  });
  function onFileSelect(x) {
    setAttributes({ background: x.url });
  }
  const handleFileSelect = (file) => {
    console.log(file);
    setAttributes({ image: file });
  };
  const MediaButton = (open) => {
    return <Icon onClick={open} className="icon" icon={plusCircle} />;
  };
  return (
    <>
      <InspectorControls>
        <PanelBody title="Background" initalOpen={true}>
          <PanelRow>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={onFileSelect}
                value={background}
                render={({ open }) => {
                  return <Button onClick={open}>Choose Image</Button>;
                }}
              />
            </MediaUploadCheck>
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <section
        className="section-photo-gallery blur-background"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat"
        }}
      >
        <div className="fluid-container">
          <div className="content-inner">
            <Row className="align-items-center">
              <Col md="5">
                <div className="inner-content" {...innerBlocksProps}></div>
              </Col>
              <Col md="7">
                {image.length === 0 ? (
                  <ResizableBox
                    size={{ height: 300, width: 300 }}
                    className="addImageBox"
                  >
                    <MediaUploadCheck>
                      <MediaUpload
                        multiple
                        gallery
                        onSelect={(file) => handleFileSelect(file)}
                        value={1}
                        render={({ open }) => MediaButton(open)}
                      />
                    </MediaUploadCheck>
                  </ResizableBox>
                ) : (
                  <Swiper
                    // install Swiper modules
                    className="myGallery"
                    loop={true}
                    centeredSlides={true}
                    modules={[Navigation, Pagination]}
                    spaceBetween={32}
                    slidesPerView={3}
                    navigation={false}
                    pagination={false}
                    // onSwiper={(swiper) => console.log(swiper)}
                    onSlideChange={() => console.log("slide change")}
                  >
                    {image.map((item, index) => {
                      const { heading, url, id, content, buttonContent } = item;
                      return (
                        <SwiperSlide key={id} className={`slide_${index}`}>
                          <img src={url} />
                        </SwiperSlide>
                      );
                    })}
                  </Swiper>
                )}
              </Col>
            </Row>
          </div>
        </div>
      </section>
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const { background, image } = attributes;

  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save({ className: `content-gallery` }));

  return (
    <>
      <section
        className="section-photo-gallery blur-background"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div className="fluid-container">
          <div className="content-inner">
            <div className="row align-items-center">
              <div className="col-md-5">
                <div className="inner-content" {...innerBlocksProps}></div>
              </div>
              <div className="col-md-7">
                <div class="swiper myGallery">
                  <div class="swiper-wrapper">
                    {image.map((item, index) => {
                      const { url } = item;
                      return (
                        <div className="swiper-slide">
                          <img src={url} />
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
