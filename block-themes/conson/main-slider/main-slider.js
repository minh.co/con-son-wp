import { registerBlockType } from "@wordpress/blocks";
import { ConSonLogo } from "../../../assets/images/logoConSon";

import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper";
import {
  BlockControls,
  InspectorControls,
  MediaUpload,
  MediaUploadCheck,
  AlignmentToolbar,
  useBlockProps,
  useInnerBlocksProps,
  __experimentalImageSizeControl as ImageSizeControl,
  __experimentalUseColorProps as useColorProps,
  RichText,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
} from "@wordpress/block-editor";
import {
  Button,
  PanelBody,
  PanelRow,
  ResizableBox,
  RangeControl,
  ToggleControl,
} from "@wordpress/components";
import { Icon, plusCircle } from "@wordpress/icons";
import { Row, Col } from "react-bootstrap";
import Example from "../../../assets/images/portfolio.png";
registerBlockType("block-themes/main-slider", {
  apiVersion: 2,
  title: "Con Son Slider",
  category: "conson",
  icon: {
    src: ConSonLogo,
  },
  html: false,
  attributes: {
    image: { type: "array", default: [] },
    perToView: { type: "number", default: 1 },
    showNextPrev: { type: "boolean", default: true },
    showDot: { type: "boolean", default: true },
    spaceBetween: { type: "number", default: 10 },
    numberSlider: { type: "number", default: 1 },
    text: { type: "string", default: "" },
    data: { type: "object", default: { heading: "", content: "", url: "" } },
  },
  example: {
    image: [
      {
        heading: "Pho ~ the spirit of Vietnamese Food",
        content:
          "Sit tellus lobortis sed senectus vivamus molestie. Condimentum volutpat morbi facilisis quam scelerisque sapien. Et, penatibus aliquam amet tellus ",
        url: Example,
      },
    ],
  },
  supports: {
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
  },
  styles: [
    { name: "left", label: "Left", isDefault: true },
    { name: "center", label: "Center" },
    { name: "right", label: "Right" },
  ],
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent(props) {
  const blockProps = useBlockProps({ className: "conson-slider",style:{ background: `${colorProps.style.backgroundColor}` } });

  const { attributes = {}, setAttributes = () => {} } = props;
  const colorProps = useColorProps(attributes);

  const {
    image = [],
    perToView,
    showNextPrev,
    spaceBetween,
    showDot,
    numberSlider,
    text,
  } = attributes;

  const handleFileSelect = (file) => {
    let dataArray = { heading: "", content: "", url: "" };

    let newArr = file.map((val, index) => {
      return {
        ["content"]: "",
        ["heading"]: "",
        ["index"]: index,
        ["url"]: val.url,
        ["buttonContent"]: "",
      };
    });

    setAttributes({
      image: newArr,
    });
  };

  const MediaButton = (open) => {
    return <Icon onClick={open} className="icon" icon={plusCircle} />;
  };
  function handleTextChange(x, index) {
    console.log(index);
    let find = image.findIndex((val) => val.index === index);
    let newData = [...image];
    newData[find].heading = x;
    // props.setAttributes({ text: x });
    setAttributes({ image: newData });
    console.log(image);
  }
  function handleContentChange(x, index) {
    console.log(index);
    let find = image.findIndex((val) => val.index === index);
    let newData = [...image];
    newData[find].content = x;
    // props.setAttributes({ text: x });
    setAttributes({ image: newData });
  }
  function handleButtonContentChange(x, index) {
    console.log(index);
    let find = image.findIndex((val) => val.index === index);
    let newData = [...image];
    newData[find].buttonContent = x;
    // props.setAttributes({ text: x });
    setAttributes({ image: newData });
  }
  return (
    <>
      {" "}
      <InspectorControls>
        <PanelBody title="Show Button next, prev">
          <ToggleControl
            checked={showNextPrev}
            onChange={() => setAttributes({ showNextPrev: !showNextPrev })}
          />
        </PanelBody>
        <PanelBody title="Show Dot">
          <ToggleControl
            checked={showDot}
            onChange={() => setAttributes({ showDot: !showDot })}
          />
        </PanelBody>
      </InspectorControls>
      <div className="imageBlock">
        {image.length === 0 ? (
          <ResizableBox
            size={{ height: 300, width: 300 }}
            className="addImageBox"
          >
            <MediaUploadCheck>
              <MediaUpload
                multiple
                gallery
                onSelect={(file) => handleFileSelect(file)}
                value={1}
                render={({ open }) => MediaButton(open)}
              />
            </MediaUploadCheck>
          </ResizableBox>
        ) : (
          <div
            className="conson-slider"
            {...blockProps}
            
          >
            <div className="container">
              <Swiper
                // install Swiper modules
                className="conson-slider"
                loop={true}
                centeredSlides={true}
                modules={[Navigation, Pagination]}
                spaceBetween={spaceBetween}
                slidesPerView={perToView}
                navigation={showNextPrev}
                pagination={
                  showDot
                    ? {
                        clickable: true,
                        renderBullet: function (index, classname) {
                          return `<span class="${classname}">${
                            "0" + (index + 1)
                          }</span>`;
                        },
                      }
                    : false
                }
                // onSwiper={(swiper) => console.log(swiper)}
                onSlideChange={() => console.log("slide change")}
              >
                {image.map((item, index) => {
                  const { heading, url, id, content, buttonContent } = item;
                  return (
                    <SwiperSlide
                      key={id}
                      className={`slide_${index}`}
                      style={{
                        background: `${colorProps.style.backgroundColor}`,
                      }}
                    >
                      <Row className="align-items-center">
                        <Col md="6" className="content-left">
                          <RichText
                            tagName="h2"
                            // className="text-start"
                            value={heading}
                            placeholder="Enter title..."
                            onChange={(x) => handleTextChange(x, index)}
                            style={{ color: `${colorProps.style.color}` }}
                          ></RichText>
                          <RichText
                            tagName="p"
                            value={content}
                            // className="text-start"
                            placeholder="Enter content..."
                            onChange={(x) => handleContentChange(x, index)}
                          ></RichText>
                          <div className="slider-button d-flex">
                            <button
                              style={{
                                background: `${colorProps.style.color}`,
                              }}
                            >
                              <RichText
                                tagName="span"
                                value={buttonContent}
                                placeholder="Enter content..."
                                onChange={(x) =>
                                  handleButtonContentChange(x, index)
                                }
                              ></RichText>
                            </button>
                          </div>
                        </Col>
                        <Col md="6">
                          <div className="wrapper-image">
                            <img src={url} width="718" height="718" />
                          </div>
                        </Col>
                      </Row>
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            </div>
          </div>
        )}
      </div>
      {/* </div> */}
    </>
  );
}

function SaveComponent(props) {
  const { attributes = {} } = props;
  const {
    image = [],
    perToView,
    showNextPrev,
    spaceBetween,
    showDot,
    numberSlider,
    text,
  } = attributes;
  const colorProps = getColorClassesAndStyles(attributes);

  return (
    <div class="conson-slider">
      <div class="container">
        <div
          class="swiper mySwiper"
          data-view={perToView}
          data-space={spaceBetween}
        >
          <div class="swiper-wrapper">
            {image.map((item, index) => {
              const { heading, url, id, content, buttonContent } = item;
              return (
                <div
                  key={id}
                  className={`swiper-slide slide_${index}`}
                  style={{ background: `${colorProps.style.backgroundColor}` }}
                >
                  <div className="row align-items-center">
                    <div className="col-md-6 content-left">
                      <RichText.Content
                        tagName="h2"
                        value={heading}
                        style={{ color: `${colorProps.style.color}` }}
                      ></RichText.Content>
                      <RichText.Content
                        tagName="p"
                        value={content}
                      ></RichText.Content>
                      <div className="slider-button d-flex">
                        <button
                          style={{ background: `${colorProps.style.color}` }}
                        >
                          <RichText.Content
                            tagName="span"
                            value={buttonContent}
                          ></RichText.Content>
                        </button>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="wrapper-image">
                        <img src={url} width="718" height="718" />
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          {showNextPrev && (
            <>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
            </>
          )}
          {showDot && <div class="swiper-pagination"></div>}
        </div>
      </div>
    </div>
  );
}
