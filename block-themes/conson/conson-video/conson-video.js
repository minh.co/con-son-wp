import { registerBlockType, registerBlockStyle } from "@wordpress/blocks";
import {
  BlockControls,
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  AlignmentToolbar,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
  MediaUpload,
  MediaUploadCheck,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  Button,
  ResizableBox,
  Icon,
} from "@wordpress/components";
import { plusCircle } from "@wordpress/icons";
import { Row, Col } from "react-bootstrap";

import { ConSonLogo } from "../../../assets/images/logoConSon";
import BG from "../../../assets/images/backgroundVideo.png";
import play from "../../../assets/images/Play.svg";
registerBlockType("block-themes/conson-video", {
  apiVersion: 2,
  title: "Conson Video",
  category: "conson",
  icon: { src: ConSonLogo },
  html: false,
  attributes: {
    background: { type: "string", default: BG },
  },
  supports: {
    // align: ["left", "right", "center"],
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
  },

  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const { background, show } = attributes;

  const blockProps = useBlockProps({});

  // const ALLOWED_BLOCKS = ["core/video"];

  const MY_TEMPLATE = [["core/embed", {previewable:true}]];
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    // allowedBlocks: ALLOWED_BLOCKS,
    template: MY_TEMPLATE,
  });
  function onFileSelect(x) {
    setAttributes({ background: x.url });
  }
  const handleFileSelect = (file) => {
    console.log(file);
    setAttributes({ avatar: file.url });
  };
  const MediaButton = (open) => {
    return <Icon onClick={open} className="icon" icon={plusCircle} />;
  };
  return (
    <>
      <InspectorControls>
        <PanelBody title="Background" initalOpen={true}>
          <PanelRow>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={onFileSelect}
                value={background}
                render={({ open }) => {
                  return <Button onClick={open}>Choose Image</Button>;
                }}
              />
            </MediaUploadCheck>
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <section
        className="section-conson-video"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          height: "900px",
        }}
      >
        <div className="fluid-container">
          <div {...innerBlocksProps}></div>
          
        </div>
      </section>
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const { background } = attributes;

  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save({style:{display:"none"}}));

  return (
    <>
      <section
        className="section-conson-video"
        style={{
          background: `url('${background}')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          height: "900px",
        }}
      >
        <div className="fluid-container">
        <div className="play"><img src={play}/></div>
          <div {...innerBlocksProps}></div>
        </div>
      </section>
    </>
  );
}
