<?php

function register_head() {
  //add file css to save(),must have header
  wp_enqueue_style('blocktheme_file_styles', get_theme_file_uri('/build/style-index.css'));
  wp_enqueue_style('custom-google-fonts', 'https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Cormorant+Garamond:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&family=Cormorant+Upright:wght@400;500;600;700&family=Open+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap');
}

add_action('wp_enqueue_scripts', 'register_head');

function myprefix_enqueue_scripts() {
  wp_enqueue_script( 
    'my-script',
    get_stylesheet_directory_uri() . '/script.js', 
    array('jquery'),
    '1.0.0',
    true );
}

add_action( 'wp_enqueue_scripts', 'myprefix_enqueue_scripts' );

function lollypop_features() {
  register_nav_menu('headerMenu','header_menu');
  register_nav_menu('headerUser','header_user');
  
  add_theme_support('post-thumbnails');
  add_theme_support('post-description');
  //support auto responsive embeds
  add_theme_support("responsive-embeds");
  //add file css to edit()
  add_editor_style('/build/style-index.css');
}

add_action('after_setup_theme', 'lollypop_features');

// function lollypop_admin_tab_bonus() {
//   register_post_type('lollypop',array(
//     'supports' => array('title','editor','excerpt'),
//     'has_archive'=>true,
//     'public'=>true,
//     'labels'=>array(
//       'name'=>'lollypop',
//       'add_new_item'=>'Add New Events',
//       'edit_item'=>'Edit Events',
//       'all_items'=>'All Events',
//       'singular_name'=>'dasds',
//     ),
//     'menu_icon'=>'dashicons-admin-site-alt3',
//   ));
// }

// add_action('init', 'lollypop_admin_tab_bonus');

 // Adding a new category.
add_filter( 'block_categories_all' , function( $categories ) {
$categories[] = array(
  'slug'  => 'lollypop',
  'title' => 'Lollypop Design'
);
return $categories;
} );

add_filter( 'block_categories_all' , function( $categories ) {
$categories[] = array(
  'slug'  => 'conson',
  'title' => 'Con Son Design'
);
return $categories;
} );

// //unregister style of class wordpress
// add_action(
// 	'wp_default_styles',
// 	function( $styles ) {
// 		// handle array is a blacklist that want to unregister styles
// 		$handles = [ 'wp-block-library', 'wp-block' ];

// 		foreach ( $handles as $handle ) {
// 			// Search and compare with the list of registered style handles:
// 			$style = $styles->query( $handle, 'registered' );
// 			if ( ! $style ) {
// 				continue;
// 			}
// 			// Remove the style
// 			$styles->remove( $handle );
// 			// Remove path and dependencies
// 			$styles->add( $handle, false, [] );
// 		}
// 	},
// 	PHP_INT_MAX
// );


class JSXBlock {
  function __construct($name,$renderCallback = null) {
    $this->name = $name;
    $this->renderCallback = $renderCallback;
    add_action('init',[$this,'onInit']);
  }

//render UI in wordpress page
  function renderCallback($attributes,$content){
    ob_start();
    require get_theme_file_path("/block-themes/{$this->name}/{$this->name}.php");
    return ob_get_clean();
  }
//

  function onInit(){
    wp_register_script($this->name, get_stylesheet_directory_uri() . "/build/{$this->name}.js", array('wp-blocks','wp-editor'));

    $params = array(
      'editor_script' => $this->name
    );

    if($this->renderCallback){
      $params['render_callback'] = [$this, 'renderCallback'];
    }
    register_block_type("block-themes/{$this->name}",$params);
  }
}
//components
new JSXBlock('container-template');
new JSXBlock('grid-template');
new JSXBlock('flex-template');
new JSXBlock('media-image');
new JSXBlock('card-content');
new JSXBlock('slider');
new JSXBlock('small-column');
new JSXBlock("button");
new JSXBlock("input");
new JSXBlock("select");

//ConSon theme
new JSXBlock('main-title');
new JSXBlock('text-highlight');
new JSXBlock("about-us");
new JSXBlock("testimony");
new JSXBlock("main-slider");
new JSXBlock("our-laurels");
new JSXBlock("image-right");
new JSXBlock("button");
new JSXBlock("portfolio");
new JSXBlock("conson-menu");
new JSXBlock("conson-video");
new JSXBlock("photo-gallery");
new JSXBlock("blog");
new JSXBlock("book-table");
new JSXBlock("newletter");
new JSXBlock("post-content");
new JSXBlock("footer");
new JSXBlock("drink-menu");
new JSXBlock("header");

?>
